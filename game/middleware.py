class SaveAnonymousUserSessionMiddleware:
    """
    Save a session key for an anonymous user to identify in the game.
    """

    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        if request.session.session_key is None:
            request.session.save()

        response = self.get_response(request)
        return response

