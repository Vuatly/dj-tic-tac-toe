from django.contrib import admin

from .models import Game


@admin.register(Game)
class GameAdmin(admin.ModelAdmin):
    fields = [
        'id',
        'player_x',
        'player_o',
        'board',
    ]
    readonly_fields = [
        'id',
    ]
