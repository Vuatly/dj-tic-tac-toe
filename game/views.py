from .models import Game
from django.db.models import Q
from django.shortcuts import redirect
from django.views.generic import DetailView, ListView


class GameListView(ListView):
    template_name = 'game-list.html'
    queryset = Game.objects.all()
    context_object_name = 'games'


class GameDetailView(DetailView):
    model = Game
    template_name = 'game-detail.html'

    def dispatch(self, request, *args, **kwargs):
        game = self.get_object()
        player = request.session.session_key

        if Game.objects.filter(Q(player_x=player) | Q(player_o=player)).exists():
            return redirect('/')
        elif game.player_x and game.player_o:
            return redirect('/')
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        game = self.get_object()

        blocks = {k: v for k, v in enumerate(list(game.board))}
        context['blocks'] = blocks
        context['game_over'] = game.is_over
        context['player_sign'] = 'X' if not game.player_x else 'O'

        return context
