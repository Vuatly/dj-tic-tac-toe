from collections import Counter
from typing import Union

from asgiref.sync import async_to_sync
from django.db import models
from channels.layers import get_channel_layer


class NonStrippingCharField(models.CharField):
    """
    Modify default CharField, because need save many
    space symbols in database ( for game board ).
    """

    def formfield(self, **kwargs):
        kwargs['strip'] = False
        return super().formfield(**kwargs)


class Game(models.Model):
    """
    Contains all info needed to play tic-tac-toe.
    Provides methods for game and notify websocket clients.
    """

    player_x = models.CharField(max_length=256, blank=True, null=True)

    player_o = models.CharField(max_length=256, blank=True, null=True)

    board = NonStrippingCharField(
        default=" " * 9,
        max_length=9,
    )

    WINNING_CONDITIONS = [
        [0, 1, 2],
        [3, 4, 5],
        [6, 7, 8],
        [0, 3, 6],
        [1, 4, 7],
        [2, 5, 8],
        [0, 4, 8],
        [2, 4, 6],
    ]

    @property
    def is_over(self) -> Union[str, bool]:
        """
        If the game is over and there is a winner, returns 'X' or 'O'.
        If the game is a draw, it returns True.
        If the game isn't over, it returns False.

        To check if the game is over, we iterate over the list of
        winning conditions and check the board to see if it
        matches one of them.
        """

        board = list(self.board)
        for wc in self.WINNING_CONDITIONS:
            w = (board[wc[0]], board[wc[1]], board[wc[2]])
            if w == ('X', 'X', 'X'):
                return 'X'
            if w == ('O', 'O', 'O'):
                return 'O'
        # Check for draw
        if ' ' in board:
            return False
        return True

    @property
    def player_turn(self) -> str:
        """
        Returns current player turn.
        Returns O if more X in the board, else returns X.
        """

        count = Counter(self.board)
        if count.get('X', 0) > count.get('O', 0):
            return 'O'
        return 'X'

    def make_move(self, index: int) -> None:
        """
        Check if index correct and block is not filled,
        then save player move in the board.
        """

        if index < 0 or index > 8:
            raise IndexError('Invalid board index')

        if self.board[index] != ' ':
            raise ValueError('This block already filled')

        board = list(self.board)
        board[index] = self.player_turn
        self.board = ''.join(board)
        self.save(update_fields=['board'])

    def reset_board(self) -> None:
        """
        Clean board if game is over.
        """

        if self.is_over:
            self.board = ' ' * 9
            self.save(update_fields=['board'])

    def notify_ws_players(self) -> None:
        """
        Notify websocket clients (players) when board changes.
        """

        channel_layer = get_channel_layer()

        async_to_sync(channel_layer.group_send)(
            'game_%s' % self.id,
            {
                'type': 'update_client_board',
                'data': {
                    'winner': self.is_over,
                    'board': self.board,
                },
            }
        )

    def save(self, *args, **kwargs):
        if self.id is not None:
            self.notify_ws_players()
        super().save(*args, **kwargs)

    def __str__(self):
        return f'Game {self.id}'

