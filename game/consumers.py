import json
from typing import Dict, Any, Union

from channels.db import database_sync_to_async
from channels.generic.websocket import AsyncWebsocketConsumer
from loguru import logger

from .models import Game


class GameConsumer(AsyncWebsocketConsumer):
    async def connect(self) -> None:
        self.game_id = self.scope['url_route']['kwargs']['game']
        self.game_group_name = 'game_%s' % self.game_id
        self.player = self.scope['session'].session_key

        await self.channel_layer.group_add(
            self.game_group_name,
            self.channel_name
        )
        await self.save_player_to_db()

        await self.accept()

    async def disconnect(self, close_code) -> None:
        if close_code == 1001:
            await self.delete_disconnected_player_from_db()

        await self.channel_layer.group_discard(
            self.game_group_name,
            self.channel_name
        )

    async def receive(self, json_data) -> None:
        data = json.loads(json_data)

        if data.get('reset_board', None) is not None:
            await self.reset_game_board()
        elif data.get('index', None) is not None:
            await self.handle_player_move(data['index'])

    async def update_client_board(self, event: Dict[str, str]) -> None:
        data = event['data']
        await self.send(text_data=json.dumps(data))

    @database_sync_to_async
    def handle_player_move(self, index: int) -> None:
        game = Game.objects.get(pk=self.game_id)
        p_turn = game.player_turn.lower()

        if getattr(game, 'player_%s' % p_turn) == self.player:
            try:
                game.make_move(index)
            except (ValueError, IndexError) as err:
                logger.debug(err)

    @database_sync_to_async
    def save_player_to_db(self) -> None:
        game = Game.objects.get(pk=self.game_id)

        if not game.player_x:
            game.player_x = self.player
            game.save(update_fields=['player_x'])
        else:
            game.player_o = self.player
            game.save(update_fields=['player_o'])

    @database_sync_to_async
    def delete_disconnected_player_from_db(self) -> None:
        game = Game.objects.get(pk=self.game_id)

        if self.player == game.player_x:
            game.player_x = ''
            game.save(update_fields=['player_x'])
        else:
            game.player_o = ''
            game.save(update_fields=['player_o'])

    @database_sync_to_async
    def reset_game_board(self) -> None:
        game = Game.objects.get(pk=self.game_id)
        game.reset_board()
