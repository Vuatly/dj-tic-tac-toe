# Django Tic Tac Toe

This is Tic Tac Toe game in Django, using WebSockets.

You can try to play here: https://dj-tic-tac-toe.herokuapp.com/

## Architecture

* When user open any page, app (middleware.py) save his session key to identify him in game.
* When user try to join game, app (views.py) check if he now playing in another game or game is full. If not, app (consumers.py) save him to database and open ws connection.
* When user left the game, app (consumers.py) delete him from game.

## Database

* In this project I use PostgreSQL


## Run locally

If you want run this app in your machine, you should update settings.py (SECRET KEY, DATABASES, CHANNEL LAYERS, ALLOWED HOSTS) and install requirements.txt .
Then you can run it like any django app.
